<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $fillable = ['name', 'pricing', 'category_id', 'user_id', 'subcategory_id', 'short_description', 'long_description', 'icon', 'available', 'published', 'created_at', 'updated_at'];

	public function reviews()
	{
	    return $this->hasMany('App\Review');
	}

	public function images()
	{
	    return $this->hasMany('App\Image');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	// The way average rating is calculated (and stored) is by getting an average of all ratings, 
	// storing the calculated value in the rating_cache column (so that we don't have to do calculations later)
	// and incrementing the rating_count column by 1

    public function recalculateRating($rating)
    {
    	
    	$reviews = $this->reviews()->notSpam()->approved();
	    $avgRating = $reviews->avg('rating');
		$this->rating_cache = round($avgRating,1);
		$this->rating_count = $reviews->count();
    	$this->save();
    }

    public function parseHTML()
    {
    	$this->long_description= str_replace(">","<br>",$this->long_description);

    	$this->long_description = preg_replace('#\*{2}(.*?)\*{2}#', '<b>$1</b>', $this->long_description);
    }
}
