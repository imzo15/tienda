<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use App\Message;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password', 'avatar_url', 'first_name', 'last_name', 'status', 'account_type', 'social_id', 'path', 'filename',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function messagesForMe()
    {
        return $this->hasMany('App\Message', 'recipient_id', 'user_isd');
    }

    public function messagesByMe()
    {
        return $this->hasMany('App\Message', 'sender_id', 'user_id');
    }

    public function lastMessageWithUser()
    {
        $user_id = Auth::user()->id;
        $id = $this->id;
        return Message::where(function($query) use ($id, $user_id){
                $query->where('recipient_id', $id)
                        ->where('sender_id', $user_id);
            })->orWhere(function($query) use ($id, $user_id){
                $query->where('recipient_id', $user_id)
                ->where('sender_id', $id);
            })
            ->orderBy('created_at', 'desc')
            ->first(['message']);
    }

    /**
     * Finds the user in the database, if it doesnt finds it, it creates it.
     * @param  string $type type of user facebook|normal
     * @param  string $id   id of social user
     * @param  User $user user object
     * @return User
     */ 
    public static function findOrCreateSocialUser($type, $id, $user)
    {
        $userFB = User::where('account_type', $type)
        ->where('social_id', $id)
        ->first();

        if($userFB){
            return $userFB;
        }

        if($type == 'facebook'){
            $userFB = User::createFacebookUser($user);
            return $userFB;
        }

        return $user;
    }

    public static function createFacebookUser($user)
    {
        $userData = [
            'name'          => isset($user->name) ? $user->name : '',
            'username'      => isset($user->name) ? $user->name : '',
            'email'         => isset($user->email) ? $user->email : '',
            'avatar_url'    => isset($user->avatar_original) ? $user->avatar_original : '',
            'social_id'     => $user->id,
            'account_type'  => 'facebook'
        ];

        return User::create($userData);
    }

    public function getImageProfile()
    {
        return ($this->account_type == 'facebook' ? $this->avatar_url : $this->path .'/' . $this->filename);
    }
}
