<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use App\User;

class SocialAuthController extends Controller
{
    
    /**
     * Redirect to provider
     * @return redirect 
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback()
    {
        $user = Socialite::driver('facebook')->user();
        $checkUser = User::findOrCreateSocialUser('facebook', $user->id, $user);
        
        auth()->login($checkUser);
        return redirect()->to('/');
    } 
}
