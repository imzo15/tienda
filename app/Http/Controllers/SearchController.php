<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Category;
use App\Product;
use Auth;
use View;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Function to handle the search
     * @param  Request $request [parameters sent from view]
     * @return View           
     */
    public function search(Request $request)
    {
        $search = $request->search_term;
        $category_id = $request->category_id;
        if ($category_id != "") {
            $user = Auth::user();
            $categories = Category::all();
            //where (name like $search OR short_descripcion like $search) AND (category_id = $category_id)
            $products = Product::where(function($query) use ($search, $category_id){
                $query->where('name', 'like', '%'.$search.'%')
                        ->orWhere('short_description', 'like', '%'.$search.'%');
            })->where(function($query) use ($category_id){
                $query->where('category_id', '=', $category_id);
            })
            ->paginate(10);
        } else {
            $user = Auth::user();
            $categories = Category::all();
            $products = Product::where('name', 'like', '%'.$search.'%')
            ->orWhere('short_description', 'like', '%'.$search.'%')
            ->paginate(10);
        }
        
        
        return View::make('search.results')
        ->with('user', $user)
        ->with('categories', $categories)
        ->with('products', $products);
    }

    /**
     * Function to hanlde the autocomplete search input
     * @param  Request $request [parameters sent from view]
     * @return JSON           JSON object that contains the suggested products
     */
    public function autocomplete(Request $request)
    {
        $term = $request->search_term;
        $data = Product::where('name', 'like', '%'.$term.'%')->take(5)->get();
        $result = array();
        foreach ($data as $key => $value) {
            $result[] = $value->name;
        }
        return response()->json($result);
    }
}
