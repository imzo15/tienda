<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use View;
use Auth;
use DB;
use App\User;
use App\Category;
use App\Message;
use Image as ImageService;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return View
     */
    public function show($username)
    {
        $user = User::where('username', $username)->get()->first();
        if($user){
            $categories = Category::all();
            return View::make('users.show')->with('user', $user)->with('categories', $categories);
        }else{
            return Redirect('/');
        }
    }

    /**
     * Displays the profile of the current logged i user
     * @return View  [user profile view]
     */
    public function showProfile()
    {
        $user = Auth::user();
        // dd($user->products());
        $categories = Category::all();
        // $usersMessages = DB::raw('SELECT LEAST(recipient_id, sender_id), GREATEST(recipient_id, sender_id)')
        // ->where('sender_id', $user->id)
        // ->orWhere('recipient_id', $user->id)
        // ->groupBy('LEAST(recipient_id, sender_id), GREATEST(recipient_id, sender_id)')
        // ->get()
        // ->unique();
        //Users who had messaged me and I have messaged
        $usersM = 
        // DB::select('SELECT LEAST(recipient_id, sender_id) recipient_id, GREATEST(recipient_id, sender_id) sender_id FROM messages JOIN users ON users.id = recipient_id OR users.id = sender_id WHERE sender_id = ? OR recipient_id = ? GROUP BY LEAST(recipient_id, sender_id), GREATEST(recipient_id, sender_id)', [$user->id, $user->id]);
        // returns array with only properties
        DB::select('SELECT LEAST(recipient_id, sender_id) recipient_id, GREATEST(recipient_id, sender_id) sender_id FROM messages WHERE sender_id = ? OR recipient_id = ? GROUP BY LEAST(recipient_id, sender_id), GREATEST(recipient_id, sender_id)', [$user->id, $user->id]);
        $user_id_list = array();
        foreach ($usersM as $key => $value) {
            if ($user->id != $value->recipient_id) {
                $user_id_list[] = $value->recipient_id;
            } else {
                $user_id_list[] = $value->sender_id;
            }
            
        }
        // dd($user_id_list);
        // dd(array_column($user_id_list, ''));
        $usersMessages = User::find( $user_id_list );
        return View::make('users.profile')->with('user', $user)->with('categories', $categories)->with('usersMessages', $usersMessages);
    }

    public function editProfile(Request $request)
    {
        // dd($request);
        if ($request->user_id != Auth::user()->id) {
            return Redirect::back();
        }
        $validator = Validator::make($request->all(), [
            'first_name' => 'max:128',
            'last_name' => 'max:128',
            'image' => 'image|mimes:jpg,jpeg,png'
        ]);
        if ($validator->fails()) {
            return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $user = Auth::user();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        if (Input::hasFile('image')) { 
             // getting all of the post data
            $file = Input::file('image');

            $destinationPath = 'uploads/';
            // $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename = str_random(6) . rand(11111,99999); // renameing image
            $fullname_image = $destinationPath.$filename.'.'.$extension;
            $upload_success = $file->move($destinationPath, $filename.'.'.$extension);
            // $upload_success = ImageService::make($file)->resize(null, 720, function($constraint){
            //     $constraint->aspectRatio();
            // })->save($destinationPath.$filename.'.'.$extension, 100);
            if(!file_exists($destinationPath."thumb/")){
                mkdir($destinationPath."thumb/", 0777, true);
            }
            $thumb = ImageService::make($fullname_image)->resize(128, 128)->save($destinationPath. "thumb/" .$filename.'.'.$extension, 90);
            $user->path = $destinationPath;
            $user->filename = $filename.'.'.$extension;
        }
        $user->save();
        Session::flash('success', 'Profile saved successfully!'); 
        return Redirect::back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
