<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Message;
use Request as ARequest;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->user_id == Auth::user()->id) {
            return Redirect::back();
        }
        $validator = Validator::make($request->all(), [
            'message' => 'required|max:254',
        ]);
        // dd($request->all());
        if ($validator->fails()) {
            return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $values = array(
            'recipient_id' => $request->user_id, 
            'sender_id' => Auth::user()->id, 
            'message' => $request->message, 
        );
        $message = Message::create($values);
        Session::flash('success', 'Message sent successfully!'); 
        return Redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get the chat conversation of a user
     * @param  Request $request 
     * @return JSON           JSON with all messages from a conversation
     */
    public function chat(Request $request)
    {
        $user_id = Auth::user()->id;
        $id = $request->user_id;
        return Message::where(function($query) use ($id, $user_id){
                $query->where('recipient_id', $id)
                        ->where('sender_id', $user_id);
            })->orWhere(function($query) use ($id, $user_id){
                $query->where('recipient_id', $user_id)
                ->where('sender_id', $id);
            })
            ->orderBy('created_at', 'desc')
            ->take(20)
            ->get();

    }

    public function storeMessageAJAX(Request $request){
        if(ARequest::ajax()){
            $validator = Validator::make($request->all(), [
                'message' => 'required|max:254',
            ]);
            // dd($request->all());
            if ($validator->fails()) {
                return Redirect::back()
                            ->withErrors($validator)
                            ->withInput();
            }
            $values = array(
                'recipient_id' => $request->user_id, 
                'sender_id' => Auth::user()->id, 
                'message' => $request->message, 
            );
            $message = Message::create($values);
            //Obtener mensajes
            $user_id = Auth::user()->id;
            $id = $request->user_id;
            return Message::where(function($query) use ($id, $user_id){
                    $query->where('recipient_id', $id)
                            ->where('sender_id', $user_id);
                })->orWhere(function($query) use ($id, $user_id){
                    $query->where('recipient_id', $user_id)
                    ->where('sender_id', $id);
                })
                ->orderBy('created_at', 'desc')
                ->get();
        }else{
            return 'fail';
        }
    }

        public function getMessagesAJAX(Request $request){
        if(ARequest::ajax()){
            $validator = Validator::make($request->all(), [
                'user_id' => 'exists:users,id',
                'user_name' => 'exists:users,name',
            ]);
            // dd($request->all());
            // return $request->user_name;
            if ($validator->fails()) {
                return Redirect::back()
                            ->withErrors($validator)
                            ->withInput();
            }
            //Obtener mensajes
            $user_id = Auth::user()->id;
            $id = $request->user_id;
            return Message::where(function($query) use ($id, $user_id){
                    $query->where('recipient_id', $id)
                            ->where('sender_id', $user_id);
                })->orWhere(function($query) use ($id, $user_id){
                    $query->where('recipient_id', $user_id)
                    ->where('sender_id', $id);
                })
                ->orderBy('created_at', 'desc')
                ->get();
        }else{
            return 'fail';
        }
    }
}
