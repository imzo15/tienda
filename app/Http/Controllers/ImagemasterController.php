<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Imagemaster;
use App\Product;
use App\Category;

class ImagemasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $imagemaster = Imagemaster::all();
        $categories = Category::all();
        return View::make('imagemaster.index', array('imagemaster'=>$imagemaster, 'categories'=>$categories));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $categories = Category::all();
        return View::make('imagemaster.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Input::hasFile('image')) { 
             // getting all of the post data
            $files = Input::file('image');
            // Making counting of uploaded images
            $file_count = count($files);
            // start count how many uploaded
            $uploadcount = 0;
            foreach($files as $file) {
                $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
                $validator = Validator::make(array('file'=> $file), $rules);
                if($validator->passes()){
                    $destinationPath = 'uploads';
                    // $filename = $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension(); // getting image extension
                    $filename = rand(11111,99999); // renameing image
                    $upload_success = $file->move($destinationPath, $filename.'.'.$extension);
                    $uploadcount ++;
                    $image =  new Imagemaster;
                    $image->path =  $destinationPath;
                    $image->imagename = $filename;
                    $image->ext = $extension;
                    $image->save();
                    
                }
            }
            if($uploadcount == $file_count){
                Session::flash('success', 'Success!'); 
                return Redirect::to('/');
            } 
            else {
                // return Redirect::to('/jrz/images')->withInput()->withErrors($validator);
                return Redirect::to('/');
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $user = Auth::user();
        if($user->name === "admin" && $user->email === "admin@email.com"){

            Imagemaster::destroy($request->image_id);   
            return Redirect::to('/jrz/imagesall');
        }else{
            return Redirect::to('/');
        }
    }
}
