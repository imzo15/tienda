<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Category;
use App\Subcategory;
use Auth;
use View;

class SubcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subcategories = Subcategory::all();
        $categories = Category::all();
        return View::make('subcategories.index', array('subcategories'=>$subcategories, 'categories'=>$categories));
    }

    /**
     * Display a listing of subcategories of the category id provided
     * @param  int $category_id country id
     * @return JSON
     */
    public function showByCategory($category_id)
    {
        return Subcategory::where('category_id', '=', $category_id)->get(['id', 'name']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return View::make('subcategories.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:6',
            'category_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return redirect('/jrz/subcategory')
            ->withErrors($validator)
            ->withInput();
        }
        $subcategory = Subcategory::create($request->all());
        Session::flash('success', 'Success!'); 
        return Redirect::to('/jrz/subcategories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
            $validator = Validator::make($request->all(), [
                'name' => 'required|min:6',
            ]);

            if ($validator->fails()) {
                return redirect('/jrz/subcategory')
                ->withErrors($validator);
            }
            $subcategory = Subcategory::find($request->subcategory_id);
            $subcategory->name = $request->name;
            $subcategory->save();
            return Redirect::to('/jrz/subcategories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Subcategory::destroy($request->subcategory_id);   
        return Redirect::to('/jrz/subcategories');
    }

}
