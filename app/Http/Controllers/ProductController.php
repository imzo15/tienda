<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Image;
use App\Category;
use App\Subcategory;
use App\Product;
use Image as ImageService;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $products = Product::all();
        return View::make('products.index', array('products'=>$products, 'categories'=>$categories));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            $categories = Category::all();
            return View::make('products.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $this->validate($request, [
        ]);
        $request->request->add(['user_id' => $user->id]);
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'pricing' => 'required|numeric',
            'icon' => 'required',
            'available' => 'required|numeric',
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'short_description' => 'required|min:10',
            'long_description' => 'required|min:20',
            'image' => 'required',
            'image.*' => 'image|mimes:jpg,jpeg,png'
        ]);
        // dd($request->all());
        if ($validator->fails()) {
            return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        }
        // dd($request->all());
        $product = Product::create($request->all());
        if (Input::hasFile('image')) { 
             // getting all of the post data
            $files = Input::file('image');
            // Making counting of uploaded images
            $file_count = count($files);
            // start count how many uploaded
            $uploadcount = 0;
            foreach($files as $file) {
                // $rules = array('file' => 'required|image|mimes:jpg,png'); //'required|mimes:png,gif'
                // $rules = [];

                // $imageRules = array(
                //     'image' => 'image|max:2000'
                // );
                //$validator = Validator::make($file, $imageRules);
                $destinationPath = 'uploads/';
                // $filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension(); // getting image extension
                $filename = str_random(10) . rand(11111,99999); // renameing image
                $fullname_image = $destinationPath.$filename.'.'.$extension;
                $upload_success = $file->move($destinationPath, $filename.'.'.$extension);
                // $upload_success = ImageService::make($file)->resize(null, 720, function($constraint){
                //     $constraint->aspectRatio();
                // })->save($destinationPath.$filename.'.'.$extension, 100);
                if(!file_exists($destinationPath."thumb/")){
                    mkdir($destinationPath."thumb/", 0777, true);
                }
                $thumb = ImageService::make($fullname_image)->resize(240, 160)->save($destinationPath. "thumb/" .$filename.'.'.$extension, 90);
                $uploadcount ++;
                $image =  new Image;
                $image->product_id = $product->id;
                $image->path =  $destinationPath;
                $image->imagename = $filename;
                $image->ext = $extension;
                $image->save();
                if($uploadcount == 1){
                    $product->icon = $destinationPath .'thumb/'. $filename.'.'.$extension;
                    $product->save();
                }
                // dd($validator->messages());
            }
            if($uploadcount == $file_count){
                Session::flash('success', 'Success!'); 
                return Redirect::to('/');
            } 
            

        }
        Session::flash('success', 'Success!'); 
        return Redirect::to('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function showBySubcategory($subcategory_id='')
    {
        $products = Product::where('subcategory_id', $subcategory_id)->get();
        $categories = Category::all();
        return View::make('products.subcategory')->with('products', $products)->with('categories', $categories);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = Auth::user();
        if($user->name === "admin" && $user->email === "admin@email.com"){
            $this->validate($request, [
            ]);
            $request->request->add(['user_id' => $user->id]);
            // dd($request->all());
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                // 'pricing' => 'required|numeric',
                // 'icon' => 'required',
                // 'available' => 'required|numeric',
                // 'category_id' => 'required',
                // 'subcategory_id' => 'required',
                // 'short_description' => 'required|min:10',
                // 'long_description' => 'required|min:20',
                // 'image' => 'required',
                // 'image.*' => 'image|mimes:jpg,jpeg,png'
            ]);
            // dd($request->all());
            if ($validator->fails()) {
                return Redirect::back()
                            ->withErrors($validator)
                            ->withInput();
            }
            $product = Product::find($request->product_id);
            $product->name = $request->name;
            $product->pricing = $request->pricing;
            $product->available = $request->available;
            $product->short_description = $request->short_description;
            $product->long_description = $request->long_description;
            $product->save();
            return Redirect::back();
        }else{
            return Redirect::to('/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $user = Auth::user();
        if($user->name === "admin" && $user->email === "admin@email.com"){
            $images = Image::select('');
            $product = Product::find($request->product_id);
            $product->images->each(function($image, $key){
                $image->delete();
            });  
            Product::destroy($request->product_id);   
            return Redirect::back();
        }else{
            return Redirect::to('/');
        }
    }
}
