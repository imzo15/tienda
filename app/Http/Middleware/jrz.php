<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Redirect;

class jrz
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if($user->name === "admin" && $user->email === "admin@email.com"){
            return $next($request);
        }else{
            return Redirect::to('/');
        }
    }
}
