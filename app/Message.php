<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
	protected $fillable = ['recipient_id', 'sender_id', 'message', 'created_at', 'updated_at'];

    public function recipient()
    {
        return $this->hasOne('App\User', 'id', 'recipient_id');
    }

    public function sender()
    {
        return $this->hasOne('App\User', 'id', 'sender_id');
    }

    // Attribute presenters
    public function getHelloAttribute()
    {
    	return "hello";
    }
    
}
