<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $fillable = ['name', 'category_id', 'created_at', 'updated_at'];
    
    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
