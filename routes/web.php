<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
*/
use App\Product;
use App\Category;
use App\Imagemaster;
use App\Review;
use Illuminate\Support\Facades\Input;
use App\jrz;

// Route for Homepage - displays all products from the shop
Route::get('/', function()
{
	$imagesmaster = Imagemaster::all();
	$categories = Category::all();
	$products = Product::all();
	return View::make('index', array('products'=>$products, 'imagesmaster'=>$imagesmaster, 'categories'=>$categories));
});

// Route that shows an individual product by its ID
Route::get('products/{id}', function($id)
{
	$product = Product::find($id);
	$categories = Category::all();
	$product->views += 1;
	$product->save();
	$product->parseHTML();
	// Get all reviews that are not spam for the product and paginate them
	$reviews = $product->reviews()->with('user')->approved()->notSpam()->orderBy('created_at','desc')->paginate(100);

	return View::make('products.single', array('product'=>$product,'reviews'=>$reviews,'categories'=>$categories));
});

// Route that handles submission of review - rating/comment
Route::post('products/{id}', array('before'=>'csrf', function($id)
{
	$input = array(
		'comment' => Input::get('comment'),
		'rating'  => Input::get('rating')
	);
	// instantiate Rating model
	$review = new Review;

	// Validate that the user's input corresponds to the rules specified in the review model
	$validator = Validator::make( $input, $review->getCreateRules());

	// If input passes validation - store the review in DB, otherwise return to product page with error message 
	if ($validator->passes()) {
		$review->storeReviewForProduct($id, $input['comment'], $input['rating']);
		return Redirect::to('products/'.$id.'#reviews-anchor')->with('review_posted',true);
	}
	
	return Redirect::to('products/'.$id.'#reviews-anchor')->withErrors($validator)->withInput();
}));

Route::group(['middleware' => ['auth','jrz']], function () {
	Route::get('/jrz/product', 'ProductController@create');
	Route::get('/jrz/products', 'ProductController@index');

	Route::get('/jrz/images', 'ImagemasterController@create');
	Route::post('/jrz/images', 'ImagemasterController@store');
	Route::get('/jrz/imagesall', 'ImagemasterController@index');
	Route::post('/jrz/images/delete', 'ImagemasterController@destroy');

	Route::get('/jrz/categories', 'CategoryController@index');
	Route::get('/jrz/category', 'CategoryController@create');
	Route::post('/jrz/category', 'CategoryController@store');
	Route::post('/jrz/category/edit', 'CategoryController@update');
	Route::post('/jrz/categories/delete', 'CategoryController@destroy');

	Route::get('/jrz/subcategories', 'SubcategoryController@index');
	Route::get('/jrz/subcategory', 'SubcategoryController@create');
	Route::post('/jrz/subcategory', 'SubcategoryController@store');
	Route::post('/jrz/subcategory/edit', 'SubcategoryController@update');
	Route::post('/jrz/subcategories/delete', 'SubcategoryController@destroy');
});

Route::group(['middleware' => ['auth']], function () {

	Route::post('/jrz/product', 'ProductController@store');
	Route::post('/jrz/product/edit', 'ProductController@update');
	Route::post('/jrz/products/delete', 'ProductController@destroy');

	/*AJAX*/
	Route::post('/sendMessage', 'MessageController@storeMessageAJAX');
	Route::post('/getMessages', 'MessageController@getMessagesAJAX');

	Route::post('/message', 'MessageController@store');
	Route::post('/message/user', 'MessageController@chat');
	Route::get('/profile', 'UserController@showProfile');
	Route::post('/profile/edit', 'UserController@editProfile');
});

	Route::get('/products/subcategory/{id}', 'ProductController@showBySubcategory');
	Route::get('/user/{username}', 'UserController@show');

Auth::routes();

Route::get('/search', 'SearchController@search');
Route::get('/autocomplete', array('as'=>'autocomplete', 'uses'=>'SearchController@autocomplete'));


Route::get('auth/facebook', [
	'as' 	=> 'auth-facebook',
	'uses'	=> 'SocialAuthController@redirectToProvider'
]);

Route::get('auth/facebook/callback', [
	'as'	=> 'facebook-callback',
	'uses'	=> 'SocialAuthController@handleProviderCallback'
]);

Route::resource('product', 'ProductController');

/* AJAX */

Route::get('subcategories/subcategoriesByCategory/{category_id}',[
	'uses' 	=> 'SubcategoryController@showByCategory',
	'as' 	=> 'subcategory.showByCategory',
]);

