@extends('layout')

@section('content')
    @if(Session::has('success'))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <h2>{!! Session::get('success') !!}</h2>
        </div>
    @endif
    <div class="row">
        <div class="col-md-3">

            <ul class="nav nav-pills nav-stacked" id="stacked-menu">
            @foreach($categories as $category)
                <li>
                    <a class="nav-container" data-toggle="collapse" data-parent="#stacked-menu" href="#p{{$loop->iteration}}">{{$category->name}}<span class="caret arrow"></span></a>    
                    @if($loop->iteration == 1)
                        <ul class="nav nav-pills nav-stacked collapse in" id="p{{$loop->iteration}}">
                    @else
                        <ul class="nav nav-pills nav-stacked collapse" id="p{{$loop->iteration}}">
                    @endif
                    @foreach($category->subcategories as $subcategory)
                        <li><a href="/products/subcategory/{{$subcategory->id}}">{{$subcategory->name}}</a></li>
                    @endforeach
                        </ul>
                </li>
            @endforeach
            </ul>


        </div>
        {{-- <div class="col-md-1"></div> --}}
        <div class="col-md-9">
            <div class="row">
            @foreach($products as $product)
                <div class="col-sm-4 col-lg-4 col-md-4">
                  <div class="thumbnail">
                    <img src="{{$product->icon}}" alt="" style="max-height: 250px; max-width: 100%;">
                    <div class="caption">
                        <div class="row">
                            <div class="col-sm-9 col-md-6">
                                <h4 class="scrollEffectText" ><span class="overflowContent h4" style="margin-left: 0px;"><a href="{{url('products/'.$product->id)}}" style="text-overflow: ellipsis; text-decoration: none !important;">{{$product->name}}</a></span></h4>
                            </div>
                            <div class="col-sm-3 col-md-6 price-tag">
                                <h4 >${{ number_format($product->pricing, 2)}}</h4>
                            </div>
                        </div>
                        <p>{{$product->short_description}}</p>
                    </div>
                    <div class="ratings">
                        <p class="pull-right">{{$product->rating_count}} {{  \Illuminate\Support\Str::plural('review', $product->rating_count)}}</p>
                        <p>
                            @for ($i=1; $i <= 5 ; $i++)
                                <span class="glyphicon glyphicon-star{{ ($i <= $product->rating_cache) ? '' : '-empty'}}"></span>
                            @endfor
                        </p>
                    </div>
                  </div>
                </div>
            @endforeach
            </div>
        </div>
        {{-- <div class="col-md-1"></div> --}}
    </div>
@stop