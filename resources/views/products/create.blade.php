@extends('layout')

@section('content')
    <div class="row">
      	{!! Form::open(array('url'=>'jrz/product','method'=>'POST', 'files'=>true)) !!}
      		<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }} ">
		    	<label for="name">Nombre</label>
		    	<input type="text" class="form-control" id="name" name="name" placeholder="Nombre del producto" value="{{ Request::old('name') ? : '' }}" autofocus>
		    	@if($errors->has('name'))
                    <span class="help-block">{{ $errors->first('name') }}</span>
                @endif
		  	</div>
      		<div class="form-group {{ $errors->has('pricing') ? ' has-error' : '' }} ">
		    	<label for="pricing">Precio</label>
		    	<input type="text" class="form-control" id="pricing" name="pricing" placeholder="Precio del producto" value="{{ Request::old('pricing') ? : '' }}">
		    	@if($errors->has('pricing'))
                    <span class="help-block">{{ $errors->first('pricing') }}</span>
                @endif
		  	</div>
		  	<div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                <label for="product-categories-dropdown" class="control-label">Categorias</label>
                <br>
                <div class="btn-group product-categories-dropdown">
                    <button type="button" class="btn btn-default dropdown-toggle product-categories-dropdown-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Categorias <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="product-categories-dropdown-menu" id="product-categories-dropdown-menu">
                    @foreach($categories as $category)
				      <li role="presentation" class="menuitem-list"><a role="menuitem-a" tabindex="-1" href="#" data-id="{{$category->id}}">{{$category->name}}</a></li>
                    @endforeach 
				    </ul>
		    		<input type="hidden" class="form-control" id="category_id" name="category_id">
                </div>
                @if($errors->has('category_id'))
                    <span class="help-block">{{ $errors->first('category_id') }}</span>
                @endif
            </div> 

		  	<div class="form-group{{ $errors->has('subcategory_id') ? ' has-error' : '' }}">
                <label for="product-subcategories-dropdown" class="control-label">Subcategorias</label>
                <br>
                <div class="btn-group product-subcategories-dropdown">
                    <button type="button" class="btn btn-default dropdown-toggle product-subcategories-dropdown-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Subcategorias <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="product-subcategories-dropdown-menu" id="product-subcategories-dropdown-menu">
                    </ul>
		    		<input type="hidden" class="form-control" id="subcategory_id" name="subcategory_id">
                </div>
                @if($errors->has('subcategory_id'))
                    <span class="help-block">{{ $errors->first('subcategory_id') }}</span>
                @endif
            </div> 

      		<div class="form-group {{ $errors->has('available') ? ' has-error' : '' }} ">
		    	<label for="available">Cantidad</label>
		    	<input type="text" class="form-control" id="available" name="available" placeholder="Cantidad disponible del producto" value="{{ Request::old('available') ? : '' }}">
		    	@if($errors->has('available'))
                    <span class="help-block">{{ $errors->first('available') }}</span>
                @endif
		  	</div>
		  	<div class="form-group {{ $errors->has('short_description') ? ' has-error' : '' }} ">
		    	<label for="long_description">Descripción corta</label>
		    	<input type="text" class="form-control" id="short_description" name="short_description" placeholder="Descripción corta del producto" value="{{ Request::old('short_description') ? : '' }}">
		    	@if($errors->has('short_description'))
                    <span class="help-block">{{ $errors->first('short_description') }}</span>
                @endif
		  	</div>
      		<div class="form-group {{ $errors->has('long_description') ? ' has-error' : '' }} ">
		    	<label for="long_description">Descripción</label>
		    	<textarea class="form-control tooltip-description" title="asdasdsad" id="long_description" name="long_description" placeholder="Descripción larga del producto
		    	**Ejemplo texto negritas**
		    	>Ejemplo Salto de linea" rows="5" >{{ Request::old('long_description') ? : '' }}</textarea>
		    	@if($errors->has('long_description'))
                    <span class="help-block">{{ $errors->first('long_description') }}</span>
                @endif 
		  	</div>
  				<div class="form-group {{ $errors->has('image') ? ' has-error' : '' }} ">
			 		<label for="image">Imagenes</label>
			    	{!! Form::file('image[]', array('multiple'=>true, 'id'=>'name')) !!}
			    	@if($errors->has('image'))
	                    <span class="help-block">{{ $errors->first('image') }}</span>
	                @endif
			  	</div>
		  	@foreach($errors->all() as $error)
			  	<div class="form-group {{ $errors->has('image.' . ($loop->iteration -1 ) ) ? ' has-error' : '' }} ">
					@if($errors->has('image.' . ($loop->iteration -1 ) ))
	                    <span class="help-block">{{ $errors->first('image.' . ($loop->iteration -1 ) ) }}</span>
	                @endif 
			  	</div>
		  	@endforeach()
			
{{-- 		  	<div class="checkbox">
		    	<label>
		      	<input type="checkbox" name="published" value="1"> Publicado
	    		</label>
		  	</div> --}}

	    	<input type="hidden" class="form-control" id="icon" name="icon" value="icon">
		  	{{ csrf_field() }}
		  	<button type="submit" class="btn btn-default">Submit</button>
		{!! Form::close() !!}
    </div>
@stop
