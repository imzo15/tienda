@extends('layout')

@section('scripts')
  {{Html::script('js/expanding.js')}}
  {{Html::script('js/starrr.js')}}

  <script type="text/javascript">
    $(function(){

      // initialize the autosize plugin on the review text area
      $('#new-review').autosize({append: "\n"});

      var reviewBox = $('#post-review-box');
      var newReview = $('#new-review');
      var openReviewBtn = $('#open-review-box');
      var closeReviewBtn = $('#close-review-box');
      var ratingsField = $('#ratings-hidden');

      openReviewBtn.click(function(e)
      {
        reviewBox.slideDown(400, function()
          {
            $('#new-review').trigger('autosize.resize');
            newReview.focus();
          });
        openReviewBtn.fadeOut(100);
        closeReviewBtn.show();
      });

      closeReviewBtn.click(function(e)
      {
        e.preventDefault();
        reviewBox.slideUp(300, function()
          {
            newReview.focus();
            openReviewBtn.fadeIn(200);
          });
        closeReviewBtn.hide();
        
      });

      // If there were validation errors we need to open the comment form programmatically 
      @if($errors->first('comment') || $errors->first('rating'))
        openReviewBtn.click();
      @endif

      // Bind the change event for the star rating - store the rating value in a hidden field
      $('.starrr').on('starrr:change', function(e, value){
        ratingsField.val(value);
      });
    });
  </script>
@stop

@section('styles')
  <style type="text/css">

     /* Enhance the look of the textarea expanding animation */
     .animated {
        -webkit-transition: height 0.2s;
        -moz-transition: height 0.2s;
        transition: height 0.2s;
      }

      .stars {
        margin: 20px 0;
        font-size: 24px;
        color: #d17581;
      }
  </style>
@stop

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <h2>{!! Session::get('success') !!} <a href="/profile">Go to my messages</a></h2>
        </div>
    @endif
    <div class="row">

    <!--     <div class="col-md-3">
            <p class="lead">Shop Categories</p>
            <div class="list-group">
              <a href="#" class="list-group-item active">Electronicos</a>
              <a href="#" class="list-group-item">Categoria 1</a>
            </div>
        </div> -->
        <div class="col-md-9">

            <div class="thumbnail">
              <div class="row" style="margin-bottom:30px;">
                <div class="col-md-12">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                        @foreach($product->images as $image)
                            @if($loop->iteration == 1)
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            @else
                            <li data-target="#carousel-example-generic" data-slide-to="{{$loop->iteration - 1}}"></li>

                            @endif
                        @endforeach
                        </ol>
                        <div class="carousel-inner">
                        @foreach($product->images as $image)
                            @if($loop->iteration == 1)
                            <div class="item active">
                            @else
                            <div class="item">
                            @endif
                            <img alt="First slide"  src="{{url('/')}}/{{$image->path}}/{{$image->imagename}}.{{$image->ext}}" class="image-carousel">
                          </div>
                        @endforeach
                        </div>
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                          <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                          <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                </div>
            </div>
              <div class="caption-full">
                  <h4 class="pull-right">${{ number_format($product->pricing, 2)}}</h4>
                  <h4><a href="{{url('products/'.$product->id)}}">{{$product->name}}</a></h4>
                  <p>{{$product->short_description}}</p>
                  <p id="long_description">{{$product->long_description}}</p>
                  <p>Cantidad disponible: {{number_format($product->available, 0)}}</p>
                  <p><a href="/user/{{$product->user->name}}">{{$product->user->name}}</a></p>
                  <button type="button" data-toggle="modal" data-target="#send_message_modal" class="btn btn-success btn-sm">Message {{$product->user->name}}</button>

              </div>
              <div class="ratings">
                  <p class="pull-right">{{$product->rating_count}} {{ \Illuminate\Support\Str::plural('review', $product->rating_count)}}</p>
                  <p>
                    @for ($i=1; $i <= 5 ; $i++)
                      <span class="glyphicon glyphicon-star{{ ($i <= $product->rating_cache) ? '' : '-empty'}}"></span>
                    @endfor
                    {{ number_format($product->rating_cache, 1)}} stars
                  </p>
              </div>
            </div>
            <div class="well" id="reviews-anchor">
              <div class="row">
                <div class="col-md-12">
                  @if(Session::get('errors'))
                    <div class="alert alert-danger">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                       <h5>There were errors while submitting this review:</h5>
                       @foreach($errors->all('<li>:message</li>') as $message)
                          {{$message}}
                       @endforeach
                    </div>
                  @endif
                  @if(Session::has('review_posted'))
                    <div class="alert alert-success">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h5>Your review has been posted!</h5>
                    </div>
                  @endif
                  @if(Session::has('review_removed'))
                    <div class="alert alert-success">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h5>Your review has been removed!</h5>
                    </div>
                  @endif
                </div>
              </div>
              <div class="text-right">
                <a href="#reviews-anchor" id="open-review-box" class="btn btn-success btn-green">Leave a Review</a>
              </div>
              <div class="row" id="post-review-box" style="display:none;">
                <div class="col-md-12">
                  {{Form::open()}}
                  {{Form::hidden('rating', null, array('id'=>'ratings-hidden'))}}
                  {{Form::textarea('comment', null, array('rows'=>'5','id'=>'new-review','class'=>'form-control animated','placeholder'=>'Enter your review here...'))}}
                  <div class="text-right">
                    <div class="stars starrr" data-rating="{{Illuminate\Support\Facades\Input::old('rating',0)}}"></div>
                    <a href="#" class="btn btn-danger" id="close-review-box" style="display:none; margin-right:10px;"> <span class="glyphicon glyphicon-remove"></span>Cancel</a>
                    <button class="btn btn-success" type="submit">Save</button>
                  </div>
                {{Form::close()}}
                </div>
              </div>

              @foreach($reviews as $review)
              <hr>
                <div class="row">
                  <div class="col-md-12">
                    @for ($i=1; $i <= 5 ; $i++)
                      <span class="glyphicon glyphicon-star{{ ($i <= $review->rating) ? '' : '-empty'}}"></span>
                    @endfor
                    
                    {!! $review->user ? '<a href="/user/'.$review->user->name.'">'. $review->user->name.'</a>' : 'Anonymous'!!} <span class="pull-right">{{$review->timeago}}</span> 
                    
                    <p>{{{$review->comment}}}</p>
                  </div>
                </div>
              @endforeach
              {{ $reviews->links() }}
            </div>
        </div>

    </div>

    <div class="modal fade" id="send_message_modal" tabindex="-1" role="dialog" aria-labelledby="send" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    <h4 class="modal-title custom_align" id="Heading">Send Message</h4>
                </div>
                {!! Form::open(array('url'=>'/message','method'=>'POST', 'files'=>true)) !!}
                <div class="modal-body">
                    <div class="form-group {{ $errors->has('message') ? ' has-error' : '' }} ">
                        <label for="message">Message</label>
                        <textarea class="form-control" title="message" id="message" name="message" placeholder="Message" rows="5" >{{ Request::old('message') ? : '' }}</textarea>
                        @if($errors->has('message'))
                            <span class="help-block">{{ $errors->first('message') }}</span>
                        @endif 
                    </div>
                </div>
                    {{ csrf_field() }}
                    <input type="hidden" id="user_id" name="user_id" value="{{$product->user->id}}" ></input>

                <div class="modal-footer ">
                    <button type="submit" class="btn btn-success" >Send</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop