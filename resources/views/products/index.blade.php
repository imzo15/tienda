@extends('layout')

@section('content')
 
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <table class="table">
                <thead class="thead-inverse">
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Precio</th>
                        <th>Cantidad</th>
                        <th>Vistas</th>
                        <th>Editar</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $product)
                    <tr>
                        <th scope="row">{{$product->id}}</th>
                        <td>{{$product->name}}</td>
                        <td>${{ number_format($product->pricing, 2)}}</td>
                        <td>{{number_format($product->available, 0)}}</td>
                        <td>{{$product->views}}</td>
                        <td><button id="delete_product" class="btn btn-warning btn-xs" data-id="{{$product->id}}" data-name="{{$product->name}}" data-pricing="{{$product->pricing}}" data-available="{{$product->available}}"  data-short_description="{{$product->short_description}}" data-long_description="{{$product->long_description}}" data-title="Edit" data-toggle="modal" data-target="#edit_product_modal"><span class="glyphicon glyphicon-pencil"></span></button></td>
                        <td><button id="delete_product" class="btn btn-danger btn-xs" data-id="{{$product->id}}" data-title="Delete" data-toggle="modal" data-target="#delete_product_modal"><span class="glyphicon glyphicon-trash"></span></button></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-1"></div>
    </div>


   <div class="modal fade" id="delete_product_modal" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    <h4 class="modal-title custom_align" id="Heading">Eliminar Producto</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Deseas borrar este producto?</div>
                </div>
                {!! Form::open(array('url'=>'jrz/products/delete','method'=>'POST', 'files'=>true)) !!}
                    {{ csrf_field() }}
                    <input type="hidden" id="product_id" name="product_id" value="" ></input>

                <div class="modal-footer ">
                    <button type="submit" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Si</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

        <div class="modal fade" id="edit_product_modal" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    <h4 class="modal-title custom_align" id="Heading">Editar categoria</h4>
                </div>
                {!! Form::open(array('url'=>'jrz/product/edit','method'=>'POST')) !!}
                <div class="modal-body">
                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }} ">
                        <label for="name">Nombre</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Nombre de la categoria" value="">
                        @if($errors->has('name'))
                            <span class="help-block">{{ $errors->first('name') }}</span>
                        @endif
                    </div>  
                    <div class="form-group {{ $errors->has('pricing') ? ' has-error' : '' }} ">
                        <label for="pricing">Precio</label>
                        <input type="text" class="form-control" id="pricing" name="pricing" placeholder="Precio del producto" value="{{ Request::old('pricing') ? : '' }}">
                        @if($errors->has('pricing'))
                            <span class="help-block">{{ $errors->first('pricing') }}</span>
                        @endif
                    </div>   
                    <div class="form-group {{ $errors->has('available') ? ' has-error' : '' }} ">
                        <label for="available">Cantidad</label>
                        <input type="text" class="form-control" id="available" name="available" placeholder="Cantidad disponible del producto" value="{{ Request::old('available') ? : '' }}">
                        @if($errors->has('available'))
                            <span class="help-block">{{ $errors->first('available') }}</span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('short_description') ? ' has-error' : '' }} ">
                        <label for="long_description">Descripción corta</label>
                        <input type="text" class="form-control" id="short_description" name="short_description" placeholder="Descripción corta del producto" value="{{ Request::old('short_description') ? : '' }}">
                        @if($errors->has('short_description'))
                            <span class="help-block">{{ $errors->first('short_description') }}</span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('long_description') ? ' has-error' : '' }} ">
                        <label for="long_description">Descripción</label>
                        <textarea class="form-control tooltip-description" title="long_description" id="long_description" name="long_description" placeholder="Descripción larga del producto" rows="5" >{{ Request::old('long_description') ? : '' }}</textarea>
                        @if($errors->has('long_description'))
                            <span class="help-block">{{ $errors->first('long_description') }}</span>
                        @endif 
                    </div>                 

                </div>
                    {{ csrf_field() }}
                    <input type="hidden" id="product_id" name="product_id" value="" ></input>

                <div class="modal-footer ">
                    <button type="submit" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Ok</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@stop

