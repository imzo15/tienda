@extends('layout')

@section('content')
 
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-4">
            <table class="table">
                <thead class="thead-inverse">
                    <tr>
                        <th>#</th>
                        <th>Imagen</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($imagemaster as $image)
                    <tr>
                        <th scope="row">{{$image->id}}</th>
                        <td><img alt="First slide" src="{{url('/')}}/{{$image->path}}/{{$image->imagename}}.{{$image->ext}}" style="width: 100%; height: 100px;"></td>
                        <td><button id="delete_image" class="btn btn-danger btn-xs" data-id="{{$image->id}}" data-title="Delete" data-toggle="modal" data-target="#delete_image_modal"><span class="glyphicon glyphicon-trash"></span></button></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-1"></div>
    </div>


   <div class="modal fade" id="delete_image_modal" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    <h4 class="modal-title custom_align" id="Heading">Eliminar imagen</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Deseas borrar esta imagen?</div>
                </div>
                {!! Form::open(array('url'=>'jrz/images/delete','method'=>'POST', 'files'=>true)) !!}
                    {{ csrf_field() }}
                    <input type="hidden" id="image_id" name="image_id" value="" ></input>

                <div class="modal-footer ">
                    <button type="submit" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span>Si</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

