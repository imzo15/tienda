@extends('layout')

@section('content')
    <div class="row">
      	{!! Form::open(array('url'=>'jrz/images','method'=>'POST', 'files'=>true)) !!}
		  	<div class="form-group">
		    	<label for="image">Imagenes</label>
		    	{!! Form::file('image[]', array('multiple'=>true, 'id'=>'name')) !!}
		    	<p class="help-block">Imagenes del encabezado master</p>
		    	<p class="errors">{!!$errors->first('images')!!}</p>
				@if(Session::has('error'))
				<p class="errors">{!! Session::get('error') !!}</p>
				@endif
		  	</div>
	  	</div>
		  	{{ csrf_field() }}
		  	<button type="submit" class="btn btn-default">Submit</button>
		{!! Form::close() !!}
    </div>
@stop
