@extends('layout')

@section('content')
    <div class="row">
    	<div class="col-md-6">
    	</div>

    	<div class="col-md-6">
      	{!! Form::open(array('url'=>'jrz/subcategory','method'=>'POST')) !!}
      		<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }} ">
		    	<label for="name">Nombre</label>
		    	<input type="text" class="form-control" id="name" name="name" placeholder="Nombre de la subcategoria" value="{{ Request::old('name') ? : '' }}" autofocus>
		    	@if($errors->has('name'))
                    <span class="help-block">{{ $errors->first('name') }}</span>
                @endif
		  	</div>

		  	<div class="form-group {{ $errors->has('category_id') ? ' has-error' : '' }} ">
		    	<label for="category_id">ID de la categoria</label>
		    	<input type="text" class="form-control" id="category_id" name="category_id" placeholder="ID de la categoria" value="{{ Request::old('category_id') ? : '' }}" autofocus data-toggle="tooltip" data-placement="top" title="ID de la categoria con la que se relaciona.">
		    	@if($errors->has('category_id'))
                    <span class="help-block">{{ $errors->first('category_id') }}</span>
                @endif
		  	</div>
      

	    	<input type="hidden" class="form-control" id="icon" name="icon" value="icon">
		  	{{ csrf_field() }}
		  	<button type="submit" class="btn btn-default">Submit</button>
		{!! Form::close() !!}
    	</div>
    </div>
@stop
