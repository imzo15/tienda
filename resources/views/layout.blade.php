<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width,height=device-height,initial-scale=1.0"/>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  
    <script type="text/javascript" src='//code.jquery.com/jquery-1.10.2.min.js'></script>
    {{Html::script('js/script.js')}}
    {{Html::script('js/jquery-ui.min.js')}}
    {{Html::script('js/moment.js')}}
    <script>
    /*** Handle jQuery plugin naming conflict between jQuery UI and Bootstrap ***/
    $.widget.bridge('uibutton', $.ui.button);
    $.widget.bridge('uitooltip', $.ui.tooltip);
</script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    {{Html::style('css/bootstrap-social.css')}}
    {{Html::style('css/font-awesome.min.css')}}
    {{Html::style('css/styles.css')}}
    {{Html::style('css/jquery-ui.min.css')}}
    {{Html::style('css/jquery-ui.structure.min.css')}}
    {{Html::style('css/jquery-ui.theme.min.css')}}
  @yield('styles')
  <style type="text/css">
    .thumbnail{ padding: 0;}

    .carousel-control, .item{
         border-radius: 4px;
     }

    .caption{
        height: 130px;
        overflow: hidden;
    } 

    .caption h4
    {
        white-space: nowrap;
    }

    .thumbnail img{
      width: 100%;
    }

    .ratings 
    {
        color: #d17581;
        padding-left: 10px;
        padding-right: 10px;
    }

    .thumbnail .caption-full {
    padding: 9px;
    color: #333;
    }

    footer{
      margin-top: 50px;
      margin-bottom: 30px;
    }
  </style>
</head>
<body>

    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '220988081662739',
          xfbml      : true,
          version    : 'v2.6'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>

    <nav class="navbar navbar-default" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{url('/')}}"><img src="/img/JRZSHOPx.png" style="max-height: 150%;" alt=""></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="navbar-collapse in" id="bs-example-navbar-collapse-1" style="height: auto;">

            <div class="col-sm-3 col-md-5">
                @if(is_null($categories))
                @else
                <form class="navbar-form" role="search" method="GET" action="{{ url('/search') }}">
                    <div class="input-group">
                        <div class="input-group-btn search-categories-dropdown">
                            <button type="button" class="btn btn-default dropdown-toggle search-categories-dropdown-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categories <span class="caret"></span></button>
                            <ul class="dropdown-menu" id="search-categories-dropdown-menu">
                                @forelse($categories as $category)
                                <li class="menuitem-list"><a href="#" data-id="{{$category->id}}">{{$category->name}}</a></li>
                                @empty
                                Empty
                                @endforelse
                            </ul>
                            <input type="hidden" class="form-control" id="search-category_id" name="category_id">
                        </div><!-- /btn-group -->
                        <input type="text" class="form-control" placeholder="Search" name="search_term" id="search_term" autofocus>

                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                    </div>
                </form>
                @endif
            </div>
            <ul class="nav navbar-nav navbar-right">
            @if (Auth::guest())
                <li><a href="{{ url('/login') }}">Login</a></li>
                <li><a href="{{ url('/register') }}">Register</a></li>
            @else
                <li class="col-sm-12 col-md-6">
                    <p class="navbar-btn"> <a href="/product/create" class="btn btn-success"><span class="glyphicon glyphicon-ok-sign"></span> Publish</a> </p>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    <span class="glyphicon glyphicon-bell" style="color:red;" aria-hidden="true"></span>
                        <span id="span-username">{{ Auth::user()->name }}</span><span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url('/profile') }}">My Profile</a></li>
                        <li>
                            <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            @endif
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>

    <div class="container" style="margin: 1%;">
      @yield('content')
      <br><br><br>


    </div>
    <footer class="footer navbar-fixed-bottom">
      <div class="container" style="background-color: white;">
        <p class="text-muted">Contacto: jrz.shop.online@gmail.com</p>
      </div>
    </footer>

    @yield('scripts')

</body>
</html>
