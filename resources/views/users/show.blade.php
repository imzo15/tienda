@extends('layout')

@section('content')
    @if(Session::has('success'))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <h2>{!! Session::get('success') !!}</h2>
        </div>
    @endif
    <div class="row">
        <div class="col-md-3">

            <ul class="nav nav-pills nav-stacked" id="stacked-menu">
            @foreach($categories as $category)
                <li>
                    <a class="nav-container" data-toggle="collapse" data-parent="#stacked-menu" href="#p{{$loop->iteration}}">{{$category->name}}<span class="caret arrow"></span></a>    
                    @if($loop->iteration == 1)
                        <ul class="nav nav-pills nav-stacked collapse in" id="p{{$loop->iteration}}">
                    @else
                        <ul class="nav nav-pills nav-stacked collapse" id="p{{$loop->iteration}}">
                    @endif
                    @foreach($category->subcategories as $subcategory)
                        <li><a href="/products/subcategory/{{$subcategory->id}}">{{$subcategory->name}}</a></li>
                    @endforeach
                        </ul>
                </li>
            @endforeach
            </ul>


        </div>
        {{-- <div class="col-md-1"></div> --}}
        <div class="col-md-9">
            <div class="row">
                <div class="col-sm-2 col-lg-2 col-md-2">
                    <div class="profile-sidebar" style="border: 1px solid #DDD; padding: 5px; margin-top: 10px;">
                        <!-- SIDEBAR USERPIC -->
                        <div class="profile-userpic">
                            <img class="media-object" src="{{$user->getImageProfile()}}" alt="64x64" style="width: 64px; height: 64px;" src="">
                        </div>
                        <!-- END SIDEBAR USERPIC -->
                        <!-- SIDEBAR USER TITLE -->
                        <div class="profile-usertitle">
                            <div class="profile-usertitle-name">{{$user->name}}</div>
                        </div>
                        <!-- END SIDEBAR USER TITLE -->
                        <!-- SIDEBAR BUTTONS -->
                        <div class="profile-userbuttons">
                            <button type="button" data-toggle="modal" data-target="#send_message_modal" class="btn btn-success btn-sm">Message</button>
                        </div>
                        <!-- END SIDEBAR BUTTONS -->
                    </div>
                </div>
                @forelse($user->products as $product)
                    <div class="col-sm-4 col-lg-4 col-md-4">
                      <div class="thumbnail">
                        <img src="/{{$product->icon}}" alt="" style="max-height: 250px; max-width: 100%;">
                        <div class="caption" style="padding: 0px;">
                            <div class="row" style="width: 100%; margin:0px;">
                                <div class="col-sm-9 col-md-6" style="padding: 0px;">
                                    <h4 class="scrollEffectText" style="padding: 5px; width: 100%;" ><span class="overflowContent h4" style="margin-left: 0px;"><a href="{{url('products/'.$product->id)}}" style="text-overflow: ellipsis; text-decoration: none !important;">{{$product->name}}</a></span></h4>
                                </div>
                                <div class="col-sm-3 col-md-6 price-tag">
                                    <h4 >${{ number_format($product->pricing, 2)}}</h4>
                                </div>
                            </div>
                            <p style="padding: 5px;">{{$product->short_description}}</p>
                        </div>
                        <div class="ratings">
                            <p class="pull-right">{{$product->rating_count}} {{  \Illuminate\Support\Str::plural('review', $product->rating_count)}}</p>
                            <p>
                                @for ($i=1; $i <= 5 ; $i++)
                                    <span class="glyphicon glyphicon-star{{ ($i <= $product->rating_cache) ? '' : '-empty'}}"></span>
                                @endfor
                            </p>
                        </div>
                      </div>
                    </div>
                @empty
                    <div class="jumbotron">
                        <div class="container">
                            <div class="col-sm-4 col-md-6">
                                <h1>Hello!</h1>
                                <p>This user has no products yet... :( <br>Send him a message!</p>
                            </div>
                        </div>
                    </div>
                @endforelse
            </div>
        </div>
        {{-- <div class="col-md-1"></div> --}}
    </div>

    <div class="modal fade" id="send_message_modal" tabindex="-1" role="dialog" aria-labelledby="send" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    <h4 class="modal-title custom_align" id="Heading">Send Message</h4>
                </div>
                {!! Form::open(array('url'=>'/message','method'=>'POST', 'files'=>true)) !!}
                <div class="modal-body">
                    <div class="form-group {{ $errors->has('message') ? ' has-error' : '' }} ">
                        <label for="message">Message</label>
                        <textarea class="form-control" title="message" id="message" name="message" placeholder="Message" rows="5" >{{ Request::old('message') ? : '' }}</textarea>
                        @if($errors->has('message'))
                            <span class="help-block">{{ $errors->first('message') }}</span>
                        @endif 
                    </div>
                </div>
                    {{ csrf_field() }}
                    <input type="hidden" id="user_id" name="user_id" value="{{$user->id}}" ></input>

                <div class="modal-footer ">
                    <button type="submit" class="btn btn-success" >Send</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@stop