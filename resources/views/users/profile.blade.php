@extends('layout')

@section('content')
    @if(Session::has('success'))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <h2>{!! Session::get('success') !!}</h2>
        </div>
    @endif
    <div class="container">

    <div class="row">
        <div class="col-md-3">
            <!-- Nav tabs -->
            <ul class="nav nav-pills nav-stacked" role="tablist">
                <li role="presentation" id="products-tab"  class="active" ><a href="#products" aria-controls="products" role="tab" data-toggle="tab">Manage products</a></li>
                <li role="presentation" id="" ><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
                <li role="presentation" id="" ><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
            </ul>
        </div>
        <div class="col-md-9 col-sm-3">
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="products">
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                            <table class="table">
                                <thead class="thead-inverse">
                                    <tr>
                                        <th>#</th>
                                        <th>Nombre</th>
                                        <th>Precio</th>
                                        <th>Cantidad</th>
                                        <th>Editar</th>
                                        <th>Eliminar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($user->products as $product)
                                    <tr>
                                        <th scope="row">{{$product->id}}</th>
                                        <td>{{$product->name}}</td>
                                        <td>${{ number_format($product->pricing, 2)}}</td>
                                        <td>{{number_format($product->available, 0)}}</td>
                                        <td><button id="delete_product" class="btn btn-warning btn-xs" data-id="{{$product->id}}" data-name="{{$product->name}}" data-pricing="{{$product->pricing}}" data-available="{{$product->available}}"  data-short_description="{{$product->short_description}}" data-long_description="{{$product->long_description}}" data-title="Edit" data-toggle="modal" data-target="#edit_product_modal"><span class="glyphicon glyphicon-pencil"></span></button></td>
                                        <td><button id="delete_product" class="btn btn-danger btn-xs" data-id="{{$product->id}}" data-title="Delete" data-toggle="modal" data-target="#delete_product_modal"><span class="glyphicon glyphicon-trash"></span></button></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-1"></div>
                    </div>


                   <div class="modal fade" id="delete_product_modal" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                    <h4 class="modal-title custom_align" id="Heading">Eliminar Producto</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Deseas borrar este producto?</div>
                                </div>
                                {!! Form::open(array('url'=>'jrz/products/delete','method'=>'POST', 'files'=>true)) !!}
                                    {{ csrf_field() }}
                                    <input type="hidden" id="product_id" name="product_id" value="" ></input>

                                <div class="modal-footer ">
                                    <button type="submit" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Si</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>

                        <div class="modal fade" id="edit_product_modal" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                    <h4 class="modal-title custom_align" id="Heading">Editar categoria</h4>
                                </div>
                                {!! Form::open(array('url'=>'jrz/product/edit','method'=>'POST')) !!}
                                <div class="modal-body">
                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }} ">
                                        <label for="name">Nombre</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Nombre de la categoria" value="">
                                        @if($errors->has('name'))
                                            <span class="help-block">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>  
                                    <div class="form-group {{ $errors->has('pricing') ? ' has-error' : '' }} ">
                                        <label for="pricing">Precio</label>
                                        <input type="text" class="form-control" id="pricing" name="pricing" placeholder="Precio del producto" value="{{ Request::old('pricing') ? : '' }}">
                                        @if($errors->has('pricing'))
                                            <span class="help-block">{{ $errors->first('pricing') }}</span>
                                        @endif
                                    </div>   
                                    <div class="form-group {{ $errors->has('available') ? ' has-error' : '' }} ">
                                        <label for="available">Cantidad</label>
                                        <input type="text" class="form-control" id="available" name="available" placeholder="Cantidad disponible del producto" value="{{ Request::old('available') ? : '' }}">
                                        @if($errors->has('available'))
                                            <span class="help-block">{{ $errors->first('available') }}</span>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('short_description') ? ' has-error' : '' }} ">
                                        <label for="long_description">Descripción corta</label>
                                        <input type="text" class="form-control" id="short_description" name="short_description" placeholder="Descripción corta del producto" value="{{ Request::old('short_description') ? : '' }}">
                                        @if($errors->has('short_description'))
                                            <span class="help-block">{{ $errors->first('short_description') }}</span>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('long_description') ? ' has-error' : '' }} ">
                                        <label for="long_description">Descripción</label>
                                        <textarea class="form-control tooltip-description" title="long_description" id="long_description" name="long_description" placeholder="Descripción larga del producto" rows="5" >{{ Request::old('long_description') ? : '' }}</textarea>
                                        @if($errors->has('long_description'))
                                            <span class="help-block">{{ $errors->first('long_description') }}</span>
                                        @endif 
                                    </div>                 

                                </div>
                                    {{ csrf_field() }}
                                    <input type="hidden" id="product_id" name="product_id" value="" ></input>

                                <div class="modal-footer ">
                                    <button type="submit" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Ok</button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                      
                </div> {{-- End panel --}}

                {{-- ------------------ Messages ------------------ --}}
                <div role="tabpanel" class="tab-pane" id="messages">
                    <div class="chat-wrapper">
    
                        <div class="row">

                            <div class="conversation-wrap col-lg-3">
                                @foreach($usersMessages as $user)
                                    <div class="media conversation message-conversation" data-id="{{$user->id}}" data-name="{{$user->name}}">
                                        <a class="pull-left" href="#">
                                            <img class="media-object" src="{{$user->getImageProfile()}}" alt="64x64" style="width: 50px; height: 50px;" src="">
                                        </a>
                                        <div class="media-body">
                                            <h5 class="media-heading">{{$user->name}}</h5>
                                            <input type="hidden" value="{{$user->id}}" id="user_id">
                                            <small>{{$user->lastMessageWithUser()['message']}}</small>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="message-wrap col-lg-8" style="min-height: 400px;">
                                <div class="msg-wrap">
                                    {{-- <div class="media msg">
                                        <a class="pull-left" href="#">
                                            <img class="media-object" data-src="holder.js/64x64" alt="64x64" style="width: 32px; height: 32px;" src="">
                                        </a>
                                        <div class="media-body">
                                            <small class="pull-right time"><i class="fa fa-clock-o"></i> 12:10am</small>
                                            <h5 class="media-heading">Naimish Sakhpara</h5>
                                            <small class="col-lg-10">Message</small>
                                        </div>
                                    </div> --}}
                                </div>


                                <div class="send-wrap ">
                                    <div class="form-group {{ $errors->has('message') ? ' has-error' : '' }} ">
                                        <textarea class="form-control" title="message" id="message" name="message" placeholder="Message" rows="5" >{{ Request::old('message') ? : '' }}</textarea>
                                        @if($errors->has('message'))
                                            <span class="help-block">{{ $errors->first('message') }}</span>
                                        @endif 
                                    </div>
                                </div>
                                <input type="hidden" id="message_user_id" name="user_id" value="" ></input>
                                <input type="hidden" id="message_user_name" name="user_name" value="" ></input>
                                <div class="btn-panel">
                                    <a id="message_user_btn" class="col-lg-4 text-right btn send-message-btn pull-right" role="button"><i class="fa fa-plus"></i> Send Message</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- ------------------ settings ------------------ --}}
                <div role="tabpanel" class="tab-pane" id="settings">
                    <h2>Edit profile</h2>
                    {!! Form::open(array('url' => 'profile/edit', 'enctype' => 'multipart/form-data')) !!}
                        <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }} ">
                            <div class="col-md-6 col-sm-6">
                                {!! Form::label('first_name', 'Name:') !!}
                                {!! Form::text('first_name', Auth::user()->first_name, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }} ">
                            <div class="col-md-6 col-sm-6">
                                {!! Form::label('last_name', 'Lastname:') !!}
                                {!! Form::text('last_name', Auth::user()->last_name, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <br><br><br><br>

                        <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }} ">
                            <div class="col-md-6 col-sm-6">
                                {!! Form::label('image', 'Profile picture:') !!}
                                {!! Form::file('image', array('id' => 'uploadImageCategory'))  !!}
                                <img id="uploadPreviewCategory" src="{{Auth::user()->path}}/{{Auth::user()->filename}}" style="width: 100px; height: 100px;" />
                            </div>
                        </div>
                        <input type="hidden" id="user_id" name="user_id" value="{{Auth::user()->id}}" ></input>
                        <br><br><br><br><br><br><br><br><br>
                        <div class="col-md-12 col-sm-12">
                            {!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
                        </div>
                    {!! Form::close() !!}
                    <hr>
                </div>
            </div>
        </div>
        {{-- <div class="col-md-1"></div> --}}
    </div>
    </div>
@stop