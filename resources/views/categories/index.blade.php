@extends('layout')

@section('content')
 
    <div class="row">
        <div class="col-md-5">
            {!! Form::open(array('url'=>'jrz/category','method'=>'POST')) !!}
                <label for="name">Nombre</label>
                <div class="input-group {{ $errors->has('name') ? ' has-error' : '' }} ">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Nombre de la categoria" value="{{ Request::old('name') ? : '' }}" autofocus>
                    @if($errors->has('name'))
                        <span class="help-block">{{ $errors->first('name') }}</span>
                    @endif
                    <span class="input-group-btn">
                        <button type="submit" style="margin-left: 20%;" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button>
                    </span>
                    <input type="hidden" class="form-control" id="icon" name="icon" value="icon">
                    {{ csrf_field() }}
                </div>

            {!! Form::close() !!}
            
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-6">
            <table class="table">
                <thead class="thead-inverse">
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Editar</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($categories as $category)
                    <tr>
                        <th scope="row">{{$category->id}}</th>
                        <td>{{$category->name}}</td>
                        <td><button id="delete_category" class="btn btn-warning btn-xs" data-id="{{$category->id}}" data-name="{{$category->name}}" data-title="Edit" data-toggle="modal" data-target="#edit_category_modal"><span class="glyphicon glyphicon-pencil"></span></button></td>
                        <td><button id="delete_category" class="btn btn-danger btn-xs" data-id="{{$category->id}}" data-title="Delete" data-toggle="modal" data-target="#delete_category_modal"><span class="glyphicon glyphicon-trash"></span></button></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>


   <div class="modal fade" id="delete_category_modal" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    <h4 class="modal-title custom_align" id="Heading">Eliminar categoria</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Deseas borrar esta categoria?</div>
                </div>
                {!! Form::open(array('url'=>'jrz/categories/delete','method'=>'POST')) !!}
                    {{ csrf_field() }}
                    <input type="hidden" id="category_id" name="category_id" value="" ></input>

                <div class="modal-footer ">
                    <button type="submit" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Si</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>


    <div class="modal fade" id="edit_category_modal" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    <h4 class="modal-title custom_align" id="Heading">Editar categoria</h4>
                </div>
                {!! Form::open(array('url'=>'jrz/category/edit','method'=>'POST')) !!}
                <div class="modal-body">
                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }} ">
                        <label for="name">Nombre</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Nombre de la categoria" value="">
                        @if($errors->has('name'))
                            <span class="help-block">{{ $errors->first('name') }}</span>
                        @endif
                    </div>    
                </div>
                    {{ csrf_field() }}
                    <input type="hidden" id="category_id" name="category_id" value="" ></input>

                <div class="modal-footer ">
                    <button type="submit" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Ok</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@stop

