@extends('layout')

@section('content')
    <div class="row">
    	<div class="col-md-6">
      	{!! Form::open(array('url'=>'jrz/category','method'=>'POST')) !!}
      		<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }} ">
		    	<label for="name">Nombre</label>
		    	<input type="text" class="form-control" id="name" name="name" placeholder="Nombre de la categoria" value="{{ Request::old('name') ? : '' }}" autofocus>
		    	@if($errors->has('name'))
                    <span class="help-block">{{ $errors->first('name') }}</span>
                @endif
		  	</div>

	    	<input type="hidden" class="form-control" id="icon" name="icon" value="icon">
		  	{{ csrf_field() }}
		  	<button type="submit" class="btn btn-default">Submit</button>
		{!! Form::close() !!}
    	</div>

    	<div class="col-md-6">
    	</div>
    </div>
@stop
