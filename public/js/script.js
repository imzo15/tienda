$(function() {


	// $(document).on("click", "#delete_product", function () {
	// 	console.log("here");
	//      var product_id = $(this).data('id');
	//      $(".modal-body #product_id").val( product_id );
 	//    	console.log(product_id);
	// });

/*	jQuery('ul.nav li.dropdown').hover(function() {
  //jQuery(this).find('.dropdown-menu').stop(true, true).show();
  jQuery(this).addClass('open');
}, function() {
  //jQuery(this).find('.dropdown-menu').stop(true, true).hide();
  jQuery(this).removeClass('open');
});


$('ul.nav > li.dropdown').click(function() {
    $(this).toggleClass('open');
});
*/
	var description = $("#long_description").text();
	console.log($.parseHTML(description));
	parsed = $.parseHTML(description);
	$("#long_description").html(parsed);
	str = "hello, <b>my name is</b> jQuery.",
  	html = $.parseHTML( str ),
    // nodeNames = [];
	
	// Append the parsed HTML
	// $("#long_description").append( html );

	$(".tooltip-description").uitooltip(
    {
        content: function(ui){
                return '<b>Negritas: </b> **Texto**<br>Salto de linea: >Texto';
            }   
    });
	//triggered when modal is about to be shown
	$('#delete_product_modal').on('show.bs.modal', function(e) {

	    //get data-id attribute of the clicked element
	    var product_id = $(e.relatedTarget).data('id');
	    console.log(product_id);
	    //populate the textbox
	    $(e.currentTarget).find('input[name="product_id"]').val(product_id);
	});

	$('#delete_image_modal').on('show.bs.modal', function(e) {

	    //get data-id attribute of the clicked element
	    var image_id = $(e.relatedTarget).data('id');
	    console.log(image_id);
	    //populate the textbox
	    $(e.currentTarget).find('input[name="image_id"]').val(image_id);
	});

	$('#delete_category_modal').on('show.bs.modal', function(e) {

	    //get data-id attribute of the clicked element
	    var category_id = $(e.relatedTarget).data('id');
	    console.log('this id: ' + category_id);
	    //populate the textbox
	    $(e.currentTarget).find('input[name="category_id"]').val(category_id);
	});

	$('#edit_category_modal').on('show.bs.modal', function(e) {

	    //get data-id attribute of the clicked element
	    var category_id = $(e.relatedTarget).data('id');
	    var category_name = $(e.relatedTarget).data('name');
	    console.log('this id: ' + category_id);
	    //populate the textbox
	    $(e.currentTarget).find('input[name="category_id"]').val(category_id);
	    $(e.currentTarget).find('input[name="name"]').val(category_name);
	    $(e.currentTarget).find('input[name="name"]').focus();
	});

	$('#edit_product_modal').on('show.bs.modal', function(e) {

	    //get data-id attribute of the clicked element
	    var product_id = $(e.relatedTarget).data('id');
	    var product_name = $(e.relatedTarget).data('name');
	    var product_pricing = $(e.relatedTarget).data('pricing');
	    var product_available = $(e.relatedTarget).data('available');
	    var product_short_description = $(e.relatedTarget).data('short_description');
	    var product_long_description = $(e.relatedTarget).data('long_description');
	    console.log('this id: ' + product_id);
	    //populate the textbox
	    $(e.currentTarget).find('input[name="product_id"]').val(product_id);
	    $(e.currentTarget).find('input[name="name"]').val(product_name);
	    $(e.currentTarget).find('input[name="pricing"]').val(product_pricing);
	    $(e.currentTarget).find('input[name="available"]').val(product_available);
	    $(e.currentTarget).find('input[name="short_description"]').val(product_short_description);
	    $(e.currentTarget).find('textarea[name="long_description"]').val(product_long_description);
	    $(e.currentTarget).find('input[name="name"]').focus();
	});

	$('#delete_subcategory_modal').on('show.bs.modal', function(e) {

	    //get data-id attribute of the clicked element
	    var subcategory_id = $(e.relatedTarget).data('id');
	    console.log('this id: ' + subcategory_id);
	    //populate the textbox
	    $(e.currentTarget).find('input[name="subcategory_id"]').val(subcategory_id);
	});

	$('#edit_subcategory_modal').on('show.bs.modal', function(e) {

	    //get data-id attribute of the clicked element
	    var subcategory_id = $(e.relatedTarget).data('id');
	    var subcategory_name = $(e.relatedTarget).data('name');
	    console.log('this id: ' + subcategory_id);
	    //populate the textbox
	    $(e.currentTarget).find('input[name="subcategory_id"]').val(subcategory_id);
	    $(e.currentTarget).find('input[name="name"]').val(subcategory_name);
	    $(e.currentTarget).find('input[name="name"]').focus();
	});

	/* INDEX PAGE */
	$('#search-categories-dropdown-menu').on('click', '.menuitem-list', function() {
		var category = $(this).text();
	    var category_id = $(this).find('a').data('id');
		var elem = $( "<span/>", {"class": "caret"});
		$('.search-categories-dropdown-btn').text(category + " ");
		$('.search-categories-dropdown-btn').append(elem);
	    $('.search-categories-dropdown').find('input[name="category_id"]').val(category_id);
	});
	/*autocomplete*/
	$('#search_term').autocomplete({
		autoFocus:true,
		source : function( request, response ) {
	        $.ajax( {
	          url: "/autocomplete",
	          data: {
	            search_term: request.term
	          },
	          success: function( data ) {
	            response( data );
	          }
	        } );
	    },
	});
	$( "#search_term" ).autocomplete( "option", "minLength", 3 );

	/* PROFILE */
	$(".message-wrap").hide();
	/* My profile */
	//Get Messages every 5 seconds
	var interval;
	$(".conversation-wrap").on("click", ".message-conversation", function(){
		interval = setInterval(getMessages, 5000);
		$('.msg-wrap').text("");
		var user_id = $(this).data("id");
		var user_name = $(this).data("name");
		console.log("id " + user_id);
		var token = $('meta[name="csrf-token"]').attr('content');
		$("#message_user_id").val(user_id);
		$("#message_user_name").val(user_name);
		$(".message-wrap").show();
        $.ajax({
            type: "POST",
            url: "/message/user",
            data: {_token:token, user_id:user_id},
            success: function(results) {
				$('#message').text("");
            	var username = $("#span-username").text();
     			$.each(results, function(index, element) {

					var dateFromNow = moment(element.created_at).fromNow(); // December 14th 2016, 11:18:55 pm
					var media_msg = $( "<div/>", {"class": "media msg"});
					var body = $( "<div/>", {"class": "media-body"});
					if (element.sender_id == user_id) {
						var small = $( "<small/>", {"class": "pull-right time", "text" : dateFromNow + " "}).append(
							$( "<i/>", {"class": "fa fa-clock-o"}
						));
						var name = $( "<h5/>", {"class": "media-heading", "text" : user_name});
						var message = $( "<div/>", {"class": "col-lg-10", "text" : element.message});

					} else {
						var small = $( "<small/>", {"class": "pull-left time", "text" : dateFromNow + " "}).append(
							$( "<i/>", {"class": "fa fa-clock-o"}
						));
						var name = $( "<h5/>", {"class": "media-heading pull-right", "text" : username});
						var message = $( "<div/>", {"class": "col-lg-10", "text" : element.message});
					}
					body.append(small);
					body.append(name);
					body.append(message);
					media_msg.append(body);
                    $('.msg-wrap').append(media_msg);
						
                });
            }
        });
	});

	$(".message-wrap").on("click", "#message_user_btn", function(){
		var user_id = $("#message_user_id").val();
		var user_name = $("#message_user_name").val();
		var message = $("#message").val();
		var token = $('meta[name="csrf-token"]').attr('content');
		if (message != "") {
	        $.ajax({
	            type: "POST",
	            url: "/sendMessage",
	            data: {_token:token, user_id:user_id, message:message},
	            success: function(results) {
	            	console.log(results);
	            	if(results != "fail"){
						$('.msg-wrap').text("");
						$('#message').val("");
						var username = $("#span-username").text();
	         			$.each(results, function(index, element) {

							var dateFromNow = moment(element.created_at).fromNow(); // December 14th 2016, 11:18:55 pm
							var media_msg = $( "<div/>", {"class": "media msg"});
							var body = $( "<div/>", {"class": "media-body"});
							if (element.sender_id == user_id) {
								var small = $( "<small/>", {"class": "pull-right time", "text" : dateFromNow + " "}).append(
									$( "<i/>", {"class": "fa fa-clock-o"}
								));
								var name = $( "<h5/>", {"class": "media-heading", "text" : user_name});
								var message = $( "<div/>", {"class": "col-lg-10", "text" : element.message});

							} else {
								var small = $( "<small/>", {"class": "pull-left time", "text" : dateFromNow + " "}).append(
									$( "<i/>", {"class": "fa fa-clock-o"}
								));
								var name = $( "<h5/>", {"class": "media-heading pull-right", "text" : username});
								var message = $( "<div/>", {"class": "col-lg-10", "text" : element.message});
							}
							body.append(small);
							body.append(name);
							body.append(message);
							media_msg.append(body);
		                    $('.msg-wrap').append(media_msg);
								
		                });
	            	}
	            }
	        });
    	}//End message != ""
	});

	function getMessages() {
		var user_id = $("#message_user_id").val();
		var user_name = $("#message_user_name").val();
		var token = $('meta[name="csrf-token"]').attr('content');
		if (user_id != "" && user_name != "") {
	
			$.ajax({
	            type: "POST",
	            url: "/getMessages",
	            data: {_token:token, user_id:user_id, user_name:user_name},
	            success: function(results) {
	            	console.log(results);
	            	if(results != "fail"){
						$('.msg-wrap').text("");
	         			
						var username = $("#span-username").text();
	         			$.each(results, function(index, element) {

							var dateFromNow = moment(element.created_at).fromNow(); // December 14th 2016, 11:18:55 pm
							var media_msg = $( "<div/>", {"class": "media msg"});
							var body = $( "<div/>", {"class": "media-body"});
							if (element.sender_id == user_id) {
								var small = $( "<small/>", {"class": "pull-right time", "text" : dateFromNow + " "}).append(
									$( "<i/>", {"class": "fa fa-clock-o"}
								));
								var name = $( "<h5/>", {"class": "media-heading", "text" : user_name});
								var message = $( "<div/>", {"class": "col-lg-10", "text" : element.message});

							} else {
								var small = $( "<small/>", {"class": "pull-left time", "text" : dateFromNow + " "}).append(
									$( "<i/>", {"class": "fa fa-clock-o"}
								));
								var name = $( "<h5/>", {"class": "media-heading pull-right", "text" : username});
								var message = $( "<div/>", {"class": "col-lg-10", "text" : element.message});
							}
							body.append(small);
							body.append(name);
							body.append(message);
							media_msg.append(body);
		                    $('.msg-wrap').append(media_msg);
		                });
	            	}
	            }
	        });
		}	
	}

	$( "#uploadImageCategory" ).change(function(e){
	    var oFReader = new FileReader();
	    oFReader.readAsDataURL(document.getElementById("uploadImageCategory").files[0]);

	    oFReader.onload = function (oFREvent) {
	        document.getElementById("uploadPreviewCategory").src = oFREvent.target.result;
	    };
	
	});
	/* END OF PROFILE */


	/* PRODUCTS */
	/* Create product */
	$('#product-categories-dropdown-menu').on('click', '.menuitem-list', function() {
		var category = $(this).text();
	    var category_id = $(this).find('a').data('id');
		var elem = $( "<span/>", {"class": "caret"});
		$('.product-categories-dropdown-btn').text(category + " ");
		$('.product-categories-dropdown-btn').append(elem);
	    $('.product-categories-dropdown').find('input[name="category_id"]').val(category_id);
        var token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            type: "GET",
            url: "/subcategories/subcategoriesByCategory/" + category_id,
            data: {_token:token},
            success: function(results) {
                $.each(results, function(index, element) {
		//<li role="presentation" class="menuitem-list"><a role="menuitem-a" tabindex="-1" href="#" data-id="{{$category->id}}">{{$category->name}}</a></li>
					var elemlist = $( "<li/>", {"class": "menuitem-list", "role": "presentation"}).append(
						$( "<a/>", {"data-id": element.id, "text" : element.name}
					));
                    $('#product-subcategories-dropdown-menu').append(elemlist);
                });
            }
        });
	});


	$('#product-subcategories-dropdown-menu').on('click', '.menuitem-list', function() {
		var subcategory = $(this).text();
	    var subcategory_id = $(this).find('a').data('id');
		var elem = $( "<span/>", {"class": "caret"});
		console.log(subcategory_id);
		$('.product-subcategories-dropdown-btn').text(subcategory + " ");
		$('.product-subcategories-dropdown-btn').append(elem);
	    $('.product-subcategories-dropdown').find('input[name="subcategory_id"]').val(subcategory_id);

	});

	$('[data-toggle="tooltip"]').tooltip();

	/* Scroll Efect for headers of tasks in kanban board */
	$('.scrollEffectText').bind({mouseenter: function() {
	        var container = $(this);
	        container.css('text-overflow', 'clip');
	        var padding = container.css('padding');
	        console.log(padding);
	        var content = container.children('span.overflowContent');
	        var offset = (container.width() + (parseInt(padding) *3 ) ) - (content.width()  );
	        if (offset < 0) {
	            content.animate({
	                'margin-left': offset
	            }, {
	                duration: offset * -10, 
	                easing: 'linear'
	            });
	        } 
	    },
	    mouseleave: function(){
	        //controller.scrollingText.reset(this);
	        var containter = $(this);
	        containter.css('text-overflow', 'ellipsis');
	        var content = containter.children('span.overflowContent');
	        var offset = containter.width() - content.width();
	        if (offset < 0) {
	            content.clearQueue().stop();
	            content.css('margin-left', 0);
	        }
	        
	    }
    });
});